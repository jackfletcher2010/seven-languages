##
##TASK 1
##
a = 1...16

a.each do |item|
  print " "
  if item % 4 == 0
    puts item
  else
    print item
  end
end

puts " "

a.each_slice(4).each do |list|
  list.each { |item| print " " + item.to_s }
  puts " "
end

##
## TASK 2
##
tree = {'grandpa' => {
          'dad' => {
            'child1' => {},
            'child2' => {}
          },
          'uncle' => {
            'child3' => {},
            'child4' => {}
          }
        }}

class Tree
  attr_accessor :children, :node_name

  def initialize(familyTree)
    @node_name = familyTree.keys[0]
    @children = []
    familyTree[@node_name].each { |n,c| @children << Tree.new(n => c) }
  end

  def visit_all(&block)
    visit &block
    children.each { |c| c.visit_all &block }
  end

  def visit(&block)
    block.call self
  end
end

family_tree = Tree.new(tree)
family_tree.visit_all { |e| puts e.node_name }

##
##TASK 3
##
def grep(filename, searchTerm)
  File.open(filename) do |file|
    file.each do |line|
      if line[searchTerm]
        puts "#{file.lineno}: #{line}"
      end
    end
  end
end

grep('foo.txt', 'out')
