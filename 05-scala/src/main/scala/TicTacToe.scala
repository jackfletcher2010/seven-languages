import scala.io.StdIn._

object TicTacToe {

  def main(args: Array[String]): Unit ={
    println("Welcome to ̶T̶i̶c̶ ̶T̶a̶c̶ ̶T̶o̶e Noughts & Crosses.")

    val ticTacToeBoard = new TicTacToeBoard
    ticTacToeBoard.gameloop()
  }

}

class TicTacToeBoard{

  private var board = (('-','-','-'), ('-','-','-'), ('-','-','-'))
  private var side = 'X'

  def gameloop(): Unit = {
    (0 to 8) foreach (_ => {
      printBoard()
      println("Player " + side + "s turn")

      var validInputProvided = false
      do {
        val (row: Int, col: Int) = readInput

        if (row > 3 || row < 0 || col > 3 || col < 0) {
          println("Row/column must be between 0 and 3")

        } else if (getTile(row, col) != '-') {
          println("Row Taken, please repick")

        } else {
          placeMove(row, col)
          checkWin(side)
          validInputProvided = true
          flipSide()
        }
      } while (!validInputProvided)
    })

    printBoard()
    println("Draw! How Boring")
    sys.exit
  }

  private def flipSide(){
    side match {
      case 'X' => side = '0'
      case '0' => side = 'X'
    }
  }

  private def readInput = {
    println("Row: ")
    val row = readInt()
    println("Column: ")
    val col = readInt()
    (row, col)
  }

  private def placeMove(row: Int, col: Int){
    board = row match {
      case 1 => board.copy(_1 = editRow(col, board._1))
      case 2 => board.copy(_2 = editRow(col, board._2))
      case 3 => board.copy(_3 = editRow(col, board._3))
    }

    def editRow(col: Int, rows: (Char, Char, Char)): (Char, Char, Char) ={
      col match {
        case 1 => rows.copy(_1 = side)
        case 2 => rows.copy(_2 = side)
        case 3 => rows.copy(_3 = side)
      }
    }
  }

  private def getTile(row: Int, col: Int): Char ={
    val columns = row match {
      case 1 => board._1
      case 2 => board._2
      case 3 => board._3
    }

    col match {
      case 1 => columns._1
      case 2 => columns._2
      case 3 => columns._3
    }
  }

  private def printBoard() {
    println(Console.GREEN + "  1 2 3")
    println(Console.GREEN +"1"+ Console.RESET +s" ${board._1._1}|${board._1._2}|${board._1._3}")
    println(Console.GREEN +"2"+ Console.RESET +s" ${board._2._1}|${board._2._2}|${board._2._3}")
    println(Console.GREEN +"3"+ Console.RESET +s" ${board._3._1}|${board._3._2}|${board._3._3}")

  }

  private def checkWin(side: Char): Unit = {
    board match {
      //Rows
      case ((`side`,`side`,`side`), (_,_,_), (_,_,_)) => printWin(side)
      case ((_,_,_), (`side`,`side`,`side`), (_,_,_)) => printWin(side)
      case ((_,_,_), (_,_,_), (`side`,`side`,`side`)) => printWin(side)

      //Columns
      case ((`side`,_,_), (`side`,_,_), (`side`,_,_)) => printWin(side)
      case ((_,`side`,_), (_,`side`,_), (_,`side`,_)) => printWin(side)
      case ((_,_,`side`), (_,_,`side`), (_,_,`side`)) => printWin(side)

      //Diags
      case ((`side`,_,_), (_,`side`,_), (_,_,`side`)) => printWin(side)
      case ((_,_,`side`), (_,`side`,_), (`side`,_,_)) => printWin(side)

      //No Win
      case _ => None
    }
  }

  private def printWin(side: Char): Unit ={
    println(side + " wins!")
    printBoard()
    sys.exit
  }

}
